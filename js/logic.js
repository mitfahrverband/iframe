let TOlink = document.querySelector(".btn.to");
let FROMlink = document.querySelector(".btn.from");
let OFFERlink = document.querySelector(".btn.offer");
let TOlist = document.querySelector(".trip-list.to");
let FROMlist = document.querySelector(".trip-list.from");
let OFFERform = document.querySelector(".offer-form");

function handleFromBtnClicked() {
    if (FROMlink.classList.contains("clicked")) {
        TOlink.style.background = "darkgrey";
        OFFERlink.style.background = "darkgrey";
        FROMlink.style.background = "white";
        TOlist.style.display = "none";
        OFFERform.style.display = "none";
        FROMlist.style.display = "flex";
    }
}

function handleToBtnClicked() {
    if (TOlink.classList.contains("clicked")) {
        TOlink.style.background = "white";
        FROMlink.style.background = "darkgrey";
        OFFERlink.style.background = "darkgrey";
        TOlist.style.display = "flex";
        FROMlist.style.display = "none";
        OFFERform.style.display = "none";
    }
}

function handleOfferBtnClicked() {
    if (OFFERlink.classList.contains("clicked")) {
        OFFERlink.style.background = "white";
        TOlink.style.background = "darkgrey";
        FROMlink.style.background = "darkgrey";
        TOlist.style.display = "none";
        FROMlist.style.display = "none";
        OFFERform.style.display = "flex";
    }

}


handleFromBtnClicked()
handleToBtnClicked()
handleOfferBtnClicked()



TOlink.onclick = function () {
    TOlink.classList.add("clicked");
    FROMlink.classList.remove("clicked");
    OFFERlink.classList.remove("clicked");
    handleToBtnClicked()
};

FROMlink.onclick = function () {
    TOlink.classList.remove("clicked");
    OFFERlink.classList.remove("clicked");
    FROMlink.classList.add("clicked");
    handleFromBtnClicked()
};

OFFERlink.onclick = function () {
    TOlink.classList.remove("clicked");
    FROMlink.classList.remove("clicked");
    OFFERlink.classList.add("clicked");
    handleOfferBtnClicked()
};

let triplistTO = document.querySelector(".trip-list.to");
let triplistFROM = document.querySelector(".trip-list.from");
let html = document.documentElement;
let body = document.body;

function saveOriginalText(triplist) {
    let textList = [];
    for (let count = 0; count < triplist.length; count++) {
        let tripText = {
            begin: triplist[count].querySelector(".begin").innerText,
            end: triplist[count].querySelector(".end").innerText,
        };
        textList.push(tripText);
    }
    return textList;
}

function deleteNull(trip) {
    let stops = trip.getElementsByClassName("stop");

    for (let count = 0; count < stops.length; count++) {
        stops[count].innerText = stops[count].innerText.replace(", null", "");
    }
}

function abbriviate(trip) {
    let tripStartText = trip.querySelector(".begin").innerText;
    let tripEndText = trip.querySelector(".end").innerText;

    if (tripStartText.length > 20) {
        tripStartText = tripStartText.replace("asse", ".");
        tripStartText = tripStartText.replace("aße", ".");
        tripStartText = tripStartText.replace("Deutschland", "DE");
        tripStartText = tripStartText.replace("Berlin", "Ber.");
        tripStartText = tripStartText.replace(", null", "");
        trip.querySelector(".begin").innerText = tripStartText;
    }

    if (tripEndText.length > 20) {
        tripEndText = tripEndText.replace("asse", ".");
        tripEndText = tripEndText.replace("aße", ".");
        tripEndText = tripEndText.replace("Deutschland", "DE");
        tripEndText = tripEndText.replace("Berlin", "Ber.");
        tripEndText = tripEndText.replace(", null", "");
        trip.querySelector(".end").innerText = tripEndText;
    }
}

for (let count = 0; count < triplistFROM.children.length; count++) {
    deleteNull(triplistFROM.children[count].children[1]);
}
for (let count = 0; count < triplistTO.children.length; count++) {
    deleteNull(triplistTO.children[count].children[1]);
}

let originalTextFROM = saveOriginalText(triplistFROM.children);
let originalTextTO = saveOriginalText(triplistTO.children);

function deAbbriviate(TOlist, FROMlist) {
    for (let count = 0; count < TOlist.length; count++) {
        TOlist[count].querySelector(".begin").innerText =
            originalTextTO[count]["begin"];
        TOlist[count].querySelector(".end").innerText =
            originalTextTO[count]["end"];
    }
    for (let count = 0; count < FROMlist.length; count++) {
        FROMlist[count].querySelector(".begin").innerText =
            originalTextFROM[count]["begin"];
        FROMlist[count].querySelector(".end").innerText =
            originalTextFROM[count]["end"];
    }
}

window.addEventListener("resize", () => {
    let docWidth = Math.max(
        html.clientWidth,
        html.scrollWidth,
        html.offsetWidth,
        body.scrollWidth,
        body.offsetWidth
    );
    if (docWidth <= 465) {
        for (let count = 0; count < triplistFROM.children.length; count++) {
            abbriviate(triplistFROM.children[count]);
        }
        for (let count = 0; count < triplistTO.children.length; count++) {
            abbriviate(triplistTO.children[count]);
        }
    } else {
        deAbbriviate(triplistTO.children, triplistFROM.children);
    }
});

window.addEventListener("load", () => {
    let docWidth = Math.max(
        html.clientWidth,
        html.scrollWidth,
        html.offsetWidth,
        body.scrollWidth,
        body.offsetWidth
    );
    if (docWidth <= 465) {
        for (let count = 0; count < triplistFROM.children.length; count++) {
            abbriviate(triplistFROM.children[count]);
        }
        for (let count = 0; count < triplistTO.children.length; count++) {
            abbriviate(triplistTO.children[count]);
        }
    } else {
        deAbbriviate(triplistTO.children, triplistFROM.children);
    }
});

for (let count = 0; count < triplistFROM.children.length; count++) {
    deleteNull(triplistFROM.children[count].children[1]);
}
for (let count = 0; count < triplistTO.children.length; count++) {
    deleteNull(triplistTO.children[count].children[1]);
}

//Scroll to top functions
window.onscroll = function () {
    buttonAppear();
};
let topBTN = document.querySelector("#top-BTN");

function buttonAppear() {
    if (document.body.scrollTop > 25 || document.documentElement.scrollTop > 25) {
        topBTN.style.display = "block";
    } else {
        topBTN.style.display = "none";
    }
}

function topFunc() {
    body.scrollTop = 0;
    html.scrollTop = 0;
}
