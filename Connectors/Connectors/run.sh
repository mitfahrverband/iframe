#!/bin/bash

jupyter nbconvert --to html --execute Connector_r2g.ipynb
jupyter nbconvert --to html --execute Connector_BBC.ipynb
jupyter nbconvert --to html --execute Connector_Mifaz.ipynb
jupyter nbconvert --to html --execute Connector_BMF.ipynb