from pykeepass import PyKeePass
import json, requests, sys
sys.path.append('../Model')
import Trip, datetime, time, copy
import pytz
from typing import List
        
# common connector utils
def getLocations(filepath):
    with open(filepath, "r") as f:
        locations=json.load(f)
    return locations


def writeFile(path, triplist):
    with open(path, 'w') as f:
        json.dump(triplist, f)
        
def getKey(filepath, master_key, portal):
    return PyKeePass(filepath, master_key).find_entries(title=portal, first=True).password

def setPortal(portal: str, trip):
    trip.portal = portal
# BBC utils
def getParamsBBC(keyOrigin:str, keyDestination:str, locations, dateStart, dateEnd, key):
    params = {
        "from_coordinate": locations[keyOrigin],
        "to_coordinate": locations[keyDestination],
        "locale": "de-DE",
        "start_date_local": dateStart.strftime("%Y-%m-%dT%H:%M"),
        "end_date_local": dateEnd.strftime("%Y-%m-%dT%H:%M"),
        "key": key,
        "radius_in_meters": 50000,
        "count": 100
    }
    return params


def getTripsIframeBBC(url, locations,dateStartToEvent,dateEndToEvent,dateStartFromEvent, dateEndFromEvent, key, center):
    results = list()
    output = dict()
    stat_dict = {}
    statistics = list()
    
    for place in locations:
        result = requests.get(url, params=getParamsBBC(center, place, locations, dateStartFromEvent, dateEndFromEvent, key)).json().get("trips")
        results+=result
        stat_dict = {"origin":center, "destination":place, "results":len(result),"start_date":dateStartFromEvent, "end_date":dateEndFromEvent}
        statistics.append(copy.deepcopy(stat_dict))
        result = requests.get(url, params=getParamsBBC(place, center, locations, dateStartToEvent, dateEndToEvent, key)).json().get("trips")
        results+=result
        stat_dict = {"origin":place, "destination":center, "results":len(result),"start_date":dateStartToEvent, "end_date":dateEndToEvent}
        statistics.append(copy.deepcopy(stat_dict))
    
    output = {"results":results, "stats":statistics}
    return output

def getTripsBBC(locations, startDate, endDate, key):
    results = list()
    params = dict()
    result_dict = dict()
    stats = list()
    trip_count = 0
    for origin in locations:
        for destination in locations:
            if origin != destination:
                trip_count = 0
                params = getParamsBBC(keyOrigin=origin, keyDestination=destination, locations=locations, dateStart=startDate, dateEnd=endDate, key=key)
                result = requests.get("https://public-api.blablacar.com/api/v3/trips", params=params).json()
                if len(result.get("trips")) != None:
                    trip_count+=len(result.get("trips"))
                    results+=result.get("trips")
                if len(result.get("trips")) == 100:
                    if result.get("next_cursor") != "":
                        params2 = params
                        params2["from_cursor"] = result.get("next_cursor")
                        result = requests.get("https://public-api.blablacar.com/api/v3/trips", params=params2).json()
                        if len(result.get("trips")) != None:
                            trip_count+=len(result.get("trips"))
                            results+=result.get("trips")
                result_dict = {"origin":origin, "destination":destination, "results":trip_count, "start_date":startDate, "end_date":endDate}
                stats.append(copy.deepcopy(result_dict))
    output = {"results":results, "stats":stats}
    return output

def getCoordinatesBBC(waypoint):
    return Trip.Coordinates(lat = waypoint['place']['latitude'], lon = waypoint['place']['longitude'])

def getStopBBC(waypoint, coordinates):
    return Trip.Stop(address=waypoint['place']['city'], coordinates=coordinates)

def makeNewTripBBC(trip, stops, portal):
    return Trip.Trip(deeplink=trip['link'], stops=stops, departTime = trip['waypoints'][0]['date_time'], portal=portal)


# Mifaz utils
def getParamsMifaz(longitudes, latitudes, startKey, goalKey, date):
    params = {
        "f":"getEntries",
        "startlatitude": latitudes[startKey],
        "startlongitude": longitudes[startKey],
        "goallatitude": latitudes[goalKey],
        "goallongitude": longitudes[goalKey],
        "tolerance": 5,
        "journeydate": date
    }
    return params

def getTripsMifaz(url, latitudes, longitudes, date):
    results = list()
    statistics = list()
    output = dict()
    for origin in latitudes:
        for destination in latitudes:
            result = requests.get(url, params=getParamsMifaz(longitudes, latitudes, origin, destination, date)).json().get('entries')
            results+=result
            statistics.append({"origin": origin, "destination": destination, "results": len(result)})
    output = {"results": results, "stats": statistics}
    return output

def getTripsMifazIframe(url, latitudes, longitudes, date, center):
    results = list()
    statistics = list()
    output = dict()
    for place in latitudes:
        result = requests.get(url, params=getParamsMifaz(longitudes, latitudes, place, center, date)).json().get('entries')
        results+=result
        statistics.append({"origin": place, "destination": center, "results": len(result)})
        result = requests.get(url, params=getParamsMifaz(longitudes, latitudes, center, place, date)).json().get('entries')
        results+=result
        statistics.append({"origin": center, "destination": place, "results": len(result)})
    output = {"results": results, "stats": statistics}
    return output

def getStartCoordinatesMifaz(entry):
     return Trip.Coordinates(lat=entry['startcoord'].split(" ")[0], lon=entry['startcoord'].split(" ")[1])
    
def getGoalCoordinatesMifaz(entry):
     return Trip.Coordinates(lat=entry['goalcoord'].split(" ")[0], lon=entry['goalcoord'].split(" ")[1])
    
def getStartLocationMifaz(entry, coordinates):
    return Trip.Stop(address=entry['startloc'], coordinates=coordinates)

def getGoalLocationMifaz(entry, coordinates):
    return Trip.Stop(address=entry['goalloc'], coordinates=coordinates)

def makeNewTripMifaz(entry, startStop, endStop, portal):
    today = datetime.datetime.today()
    if 'journeydate' in entry: 
        if entry['journeydate'] != None:
            newTrip = Trip.Trip(deeplink=entry['url'],lastUpdated=entry['lastupdate'], departDate=entry['journeydate'],departTime= None, stops=[startStop, endStop], portal=portal)
    else:
        newTrip = Trip.Trip(deeplink=entry['url'],lastUpdated=entry['lastupdate'], departDate=today,departTime= None, stops=[startStop, endStop], portal=portal)
    return newTrip
    
# BMF utils
def getParamsBMF(locations, direction, place, date):
    return {
        direction: locations[place],
        "date": date,
    }
def getTripsBMF(url, locations, date, direction):
    results = list()
    for place in locations:
            result=requests.get(url, params=getParamsBMF(locations, direction, place, date), 
                                proxies=dict(http="socks5h://localhost:1234",
                                            https="socks5h://localhost:1234")).json()         
            if len(result["resultset"]) > 0:
                results.append(result)
            time.sleep(0.5)
    return results
def getStartLocationBMF(result, trip):
    return result["places"][trip[1][1]].replace(".","").strip()

def getGoalLocationBMF(result, trip):
    return result["places"][trip[2][1]].replace(".","").strip()

def getCoordinatesBMF(address):
    coordinates = requests.get("https://photon.ride2go.com/api", params={"q": address}).json()["features"][0]["geometry"]["coordinates"]
    return Trip.Coordinates(lat=coordinates[1], lon=coordinates[0])

def makeStopBMF(address, coordinates):
    return Trip.Stop(coordinates=coordinates, address=address)

def makeNewTripBMF(stops: List[Trip.Stop], trip, day, portal):
    return Trip.Trip(deeplink = copy.deepcopy(trip[0]), 
                                stops=stops, 
                                departTime=datetime.datetime.strptime(trip[1][0], "%H:%M").time(), 
                                departDate=datetime.datetime.fromtimestamp(int(day)).astimezone(pytz.timezone('Europe/Budapest')).date(),
                                portal=portal)
# Ride2Go utils
def getParamsRide2Go(bounding_box):
    return f"""
        {{
          "southWestCoordinates":
            {{
              "lat": {bounding_box["sw_lat"]},
              "lon": {bounding_box["sw_lon"]}
            }},
         "northEastCoordinates":
            {{
              "lat": {bounding_box["ne_lat"]},
              "lon": {bounding_box["ne_lon"]}
            }},
            "lastModifiedSinceDays": 1825
        }}
        """   

def getHeadersRide2Go(key):
    return {
        'Content-type': 'text/plain;charset=UTF-8',
        'X-API-Key': key}

def getTripsRide2Go(url, bounding_box, headers):
    results = requests.get(url,data=getParamsRide2Go(bounding_box),headers=headers).json()
    trips = list()
    for trip in results:
        trips.append(copy.deepcopy(json.loads(Trip.Trip.parse_obj(trip).json())))
    return trips