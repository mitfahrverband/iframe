from datetime import datetime
from doctest import Example
from pydantic import BaseModel, Field


class Constraints(BaseModel):
    lon: float = Field(
        description="The longitude of the location",
        ge=-180,
        lt=180,
        example="13.9934706687"
        )
    lat: float = Field(
        description="The latitude of the location",
        ge=-90,
        lt=90,
        example="53.0137311391"
    )
    startDate: datetime = Field(
        description="The start of the event in question",
        example="2022-09-10T19:00"
    )
    endDate: datetime = Field(
        description="The end of the event in question",
        example="2022-09-10T19:00"
    )
    TOLabel: str = Field(
        description="a label for the TO toggle button",
        example="To Concert"
    )
    FROMLabel: str = Field(
        description="a label for the FROM toggle button",
        example="From Demo"
    )
    radius: int = Field(
        description="the radius in which we are looking for the trips [IN KMs!]"

    )
    timeWindowBefore: int = Field(
        description="The required time window before the event(in hours)"
    )
    timeWindowAfter: int = Field(
        description="The required time window after the event(in hours)"
    )

class BoundaryPoint(BaseModel):
    lat: float = Field(
        ge=-90,
        lt=90
    )
    lon: float = Field(
        ge=-180,
        lt=180
    )