from ast import parse
import datetime,json,calendar
from jinja2 import Environment, FileSystemLoader
from Models import Carpool
from pydantic import Field, parse_obj_as
from typing import List
from datetime import date, timedelta
import copy
from Models import IFrameConstraintsModel

#Global variables

filteredListFrom = list()
filteredListTo = list()
filteredListBefore = list()
filteredListAfter = list()

weekdayList = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"}

currTime = datetime.datetime.now().time()

today = datetime.datetime.today()

weekdayToday = calendar.day_name[today.weekday()].lower()

# Functions

#Location filtering
def convertKMtoDegree(radius):
    return radius/111.111

def createTopRightPoint(config):
    return IFrameConstraintsModel.BoundaryPoint(lat=config.lat+convertKMtoDegree(config.radius), lon=config.lon+convertKMtoDegree(config.radius))

def createBottomLeftPoint(config):
    return IFrameConstraintsModel.BoundaryPoint(lat=config.lat-convertKMtoDegree(config.radius), lon=config.lon-convertKMtoDegree(config.radius))

#Checks if a given location is within the radius around the event's location
def checkIfWithinBoundary(config, stop):
    topRightPoint: IFrameConstraintsModel.BoundaryPoint = createTopRightPoint(config)
    bottomLeftPoint: IFrameConstraintsModel.BoundaryPoint = createBottomLeftPoint(config)

    if stop.coordinates.lon <= topRightPoint.lon:
        if stop.coordinates.lat <= topRightPoint.lat:
            if stop.coordinates.lon >= bottomLeftPoint.lon:
                if stop.coordinates.lat >= bottomLeftPoint.lat:
                    return True
    return False

#Time filtering
def createTimeWindowBefore(config):
    return config.startDate-datetime.timedelta(hours = config.timeWindowBefore)

def createTimeWindowAfter(config):
    return config.endDate+datetime.timedelta(hours=config.timeWindowAfter)

def convertDateToWeekday(date:datetime.datetime):
    return calendar.day_name[date.weekday()].lower()

#Checks if the trip is occurring in a certain time window before the event
def checkDateBefore(trip, config):
    timeWindowBefore = createTimeWindowBefore(config)
    eventWeekday = convertDateToWeekday(config.startDate)
    beforeEventDayWeekday = convertDateToWeekday((config.startDate-datetime.timedelta(days=1)))

    if trip.departDate != None and type(trip.departTime) == datetime.time:
        tmpDate = datetime.datetime.combine(trip.departDate, trip.departTime)
        if tmpDate >= timeWindowBefore:
            if tmpDate <= config.startDate:
                return True
    elif type(trip.departTime) == datetime.datetime:
        if trip.departTime >= timeWindowBefore:
            if trip.departTime <= config.startDate:
                return True
    elif trip.departTime == None:
        return False
    elif trip.weekdays != None:
        if eventWeekday in trip.weekdays:
            if timeWindowBefore.time() < trip.departTime and beforeEventDayWeekday in trip.weekdays:
                newTrip = copy.deepcopy(trip)
                newTrip.weekdays = None
                newTrip.departDate = (config.startDate-datetime.timedelta(days=1)).date()
                filteredListTo.append(newTrip)
            if datetime.datetime.combine(config.startDate.date(),trip.departTime) >= timeWindowBefore and trip.departTime < config.startDate.time():
                return True
        if beforeEventDayWeekday in trip.weekdays:
            if datetime.datetime.combine(timeWindowBefore.date(), trip.departTime) >= timeWindowBefore:
                return True
    return False
#Gives back the date of the given trip - if recurring, then exactly the date before the event
def getDateBefore(trip, config):
    timeWindowBefore = createTimeWindowBefore(config)
    eventWeekday = convertDateToWeekday(config.startDate)
    beforeEventDayWeekday = convertDateToWeekday((config.startDate-datetime.timedelta(days=1)))


    if trip.departDate != None and type(trip.departTime) == datetime.time:
        tmpDate = datetime.datetime.combine(trip.departDate, trip.departTime)
        if tmpDate >= timeWindowBefore:
            if tmpDate <= config.startDate:
                return tmpDate
    elif type(trip.departTime) == datetime.datetime:
        if trip.departTime >= timeWindowBefore:
            if trip.departTime <= config.startDate:
                return trip.departTime
    elif trip.departTime == None:
        return None
    elif trip.weekdays != None:
        if eventWeekday in trip.weekdays:
            tmpDate = datetime.datetime.combine(config.startDate.date(),trip.departTime)
            if tmpDate >= timeWindowBefore and tmpDate < config.startDate:
                return tmpDate
        if beforeEventDayWeekday in trip.weekdays:
            tmpDate = datetime.datetime.combine(timeWindowBefore.date(), trip.departTime)
            if tmpDate >= timeWindowBefore:
                return tmpDate
    return None

#Checks if given trip occurs after the event in a certain time window
def checkDateAfter(trip, config):
    timeWindowAfter = createTimeWindowAfter(config)
    eventWeekday = convertDateToWeekday(config.endDate)
    afterEventWeekday = convertDateToWeekday((config.endDate+datetime.timedelta(days=1)))

    if trip.departDate != None and type(trip.departTime) == datetime.time:
        tmpDate = datetime.datetime.combine(trip.departDate, trip.departTime)
        if tmpDate > config.endDate and tmpDate <= timeWindowAfter:
            return True
    elif type(trip.departTime) == datetime.datetime:
            if trip.departTime > config.endDate and trip.departTime <= timeWindowAfter:
                return True
    elif trip.departTime == None:
            return False
    elif trip.weekdays != None:
        if eventWeekday in trip.weekdays:
            if trip.departTime < timeWindowAfter.time() and afterEventWeekday in trip.weekdays:
                newTrip = copy.deepcopy(trip)
                newTrip.weekdays = None
                newTrip.departDate = (config.endDate+datetime,timedelta(days=1)).date()
                filteredListFrom.append(newTrip)
            tmpDate = datetime.datetime.combine(config.endDate.date(), trip.departTime)
            if tmpDate > config.endDate and tmpDate <= timeWindowAfter:
                return True
        if afterEventWeekday in trip.weekdays:
            if trip.departTime > timeWindowAfter.time():
                tmpDate = datetime.datetime.combine((timeWindowAfter-datetime.timedelta(days=1)).date(), trip.departTime)
                if tmpDate <= timeWindowAfter:
                    return True
            tmpDate = datetime.datetime.combine(timeWindowAfter.date(), trip.departTime) 
            if tmpDate <= timeWindowAfter:
                return True
    return False

#Gives back the date of a trip - if recurring, gives back exactly the date right after the event
def getDateAfter(trip, config):
    timeWindowAfter = createTimeWindowAfter(config)
    eventWeekday = convertDateToWeekday(config.endDate)
    afterEventWeekday = convertDateToWeekday((config.endDate+datetime.timedelta(days=1)))

    if trip.departDate != None and type(trip.departTime) == datetime.time:
        tmpDate = datetime.datetime.combine(trip.departDate, trip.departTime)
        if tmpDate > config.endDate and tmpDate <= timeWindowAfter:
            return tmpDate
    elif type(trip.departTime) == datetime.datetime:
        if trip.departTime > config.endDate and trip.departTime <= timeWindowAfter:
            return trip.departTime
    elif trip.departTime == None:
        return None
    elif trip.weekdays != None:
        if eventWeekday in trip.weekdays:
            tmpDate = datetime.datetime.combine(config.endDate.date(), trip.departTime)
            if tmpDate > config.endDate and tmpDate <= timeWindowAfter:
                return tmpDate
        if afterEventWeekday in trip.weekdays:
            if trip.departTime > timeWindowAfter.time():
                tmpDate = datetime.datetime.combine((timeWindowAfter-datetime.timedelta(days=1)).date(), trip.departTime)
                if tmpDate <= timeWindowAfter:
                    return tmpDate
            else:
                tmpDate = datetime.datetime.combine(timeWindowAfter.date(), trip.departTime) 
                if tmpDate <= timeWindowAfter:
                    return tmpDate
    return None


def explodeRecurringTripBefore(config, trip, tripList):
    if trip.weekdays != None:
        eventWeekday = convertDateToWeekday(config.startDate)
        if eventWeekday in trip.weekdays:
            newTrip = copy.deepcopy(trip)
            newTrip.weekdays = None
            newTrip.departDate = config.startDate.date()
            tripList.append(copy.deepcopy(newTrip))
        tmpDate = config.startDate+datetime.timedelta(days=1)
        while tmpDate != config.startDate:
            if convertDateToWeekday(tmpDate) in trip.weekdays:
                newTrip = copy.deepcopy(trip)
                newTrip.weekdays = None
                newTrip.departDate = tmpDate.date()
                tripList.append(copy.deepcopy(newTrip))
            if convertDateToWeekday(tmpDate) == convertDateToWeekday(config.startDate) and tmpDate > config.startDate:
                tmpDate-=datetime.timedelta(days=14)
            else :
                tmpDate+=datetime.timedelta(days=1)
        return True
    return False

def explodeRecurringTripAfter(config, trip, tripList):
    if trip.weekdays != None:
        eventWeekday = convertDateToWeekday(config.endDate)
        if eventWeekday in trip.weekdays:
            newTrip = copy.deepcopy(trip)
            newTrip.weekdays = None
            newTrip.departDate = config.endDate.date()
            tripList.append(copy.deepcopy(newTrip))
        tmpDate = config.endDate+datetime.timedelta(days=1)
        while tmpDate != config.endDate:
            if convertDateToWeekday(tmpDate) in trip.weekdays:
                newTrip = copy.deepcopy(trip)
                newTrip.weekdays = None
                newTrip.departDate = tmpDate.date()
                tripList.append(copy.deepcopy(newTrip))
            if convertDateToWeekday(tmpDate) == convertDateToWeekday(config.endDate) and tmpDate > config.endDate:
                tmpDate-=datetime.timedelta(days=14)
            else :
                tmpDate+=datetime.timedelta(days=1)
        return True
    return False

#Loads json file to dictionary with the help of Carpool model
def loadFile(path: str):
    trips = list()
    with open(path, "r") as d:
        jsondata = json.load(d)
    for item in jsondata:
        trips.append(Carpool.Carpool.parse_obj(item))
    return trips

#Sets the time that will be rendered
def setTime(trip: Carpool.Carpool):
        if trip.departTime != None:
            trip.departTimeRender = trip.departTime.strftime("%H:%M")
        else:
            trip.departTimeRender = 'TBD'
            
            
def findNextWeekDay(trip, language, weekdaysLanguages):
    i = 1
    while i <= 7:
        tmpDay = calendar.day_name[(today+timedelta(days=i)).weekday()].lower()
        if tmpDay in trip.weekdays:
            trip.departDateRender = weekdaysLanguages[language][tmpDay] 
            break
        i += 1
        
def checkDepartTimeType(trip):
    if(type(trip.departTime) != datetime.time and trip.departTime != None):
                tripTime = trip.departTime.time()
    else:
        tripTime = trip.departTime
    return tripTime


#Checks if the departTime is datetime type, and if it is, sets the date which will be rendered
def checkIfTimeIsDate(trip):
    if type(trip.departTime) == datetime.datetime:
        trip.departDateRender = trip.departTime.strftime("%d.%m.")


def isOld(trip):
    boundary = today - datetime.timedelta(days=500)
    try:
        if trip.lastUpdated.date() < boundary.date():
            return True
        return False
    except AttributeError:
        return False

#Attahes the correct logo to a trip(with giving the relative filepath to the logo)
def attachLogo(trip):
    if "mifaz" in trip.deeplink:
            trip.logo = "../img/mifaz.png"
    elif "bessermitfahren" in trip.deeplink:
            trip.logo = "../img/bessermitfahren_logo.png"
    elif "blablacar" in trip.deeplink:
        trip.logo = "../img/01 - BlaBlaCar (no background).png"
    else: trip.logo = "../img/icon_ride2go_green_small.jpg"

#Renders the html file from the template
def renderFile( filename,templatesFolder, templateFile, fromList, toList, extension, config):
    fileloader = FileSystemLoader(templatesFolder)
    env = Environment(loader=fileloader)
    rendered = env.get_template(templateFile).render(fromlist = fromList, to = toList, fromLabel = config.FROMLabel, toLabel = config.TOLabel)
    fileName = f"{filename}.{extension}"
    with open(f"./iframe_html/{fileName}", "w") as f:
        f.write(rendered)

def renderFileFullList(language, templatesFolder, templateFile, tripList, extension):
    fileloader = FileSystemLoader(templatesFolder)
    env = Environment(loader=fileloader)
    rendered = env.get_template(templateFile).render(list = tripList,title=language, weekdays = weekdayList, lang = language)
    fileName = f"triplist_{language}.{extension}"
    with open(f"./htmls/{fileName}", "w") as f:
        f.write(rendered)

def renderHTML(config: IFrameConstraintsModel.Constraints, filename, inputFile):
    tripList = loadFile(inputFile)
    for trip in tripList:
        attachLogo(trip)
        if isOld(trip):
            continue
        if type(trip.departTime) == datetime.time:
            trip.departTimeRender = trip.departTime.strftime("%H:%M")
        elif type(trip.departTime) == datetime.datetime:
            trip.departTimeRender = trip.departTime.time().strftime("%H:%M")
        else :
            continue
        if checkIfWithinBoundary(config, trip.stops[0]):
            if not explodeRecurringTripAfter(config, trip, filteredListFrom):
                filteredListFrom.append(copy.deepcopy(trip))
        if(checkIfWithinBoundary(config, trip.stops[-1])):
            if not explodeRecurringTripBefore(config, trip, filteredListTo):
                filteredListTo.append(copy.deepcopy(trip))

    for trip in filteredListTo:
        if checkDateBefore(trip, config):
            trip.departDateRender = getDateBefore(trip, config).strftime("%d.%m.")
            filteredListBefore.append(copy.deepcopy(trip))

    for trip in filteredListFrom:
        if checkDateAfter(trip, config):
            trip.departDateRender = getDateAfter(trip, config).strftime("%d.%m.")
            filteredListAfter.append(copy.deepcopy(trip))
    #This part renders the html and writes it in a file
    renderFile(filename,"templates", "iframe_template.html", filteredListAfter,filteredListBefore, "html", config)