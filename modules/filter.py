from Models import Carpool
import json
import datetime
import calendar
import copy
from Models import IFrameConstraintsModel
from Connectors.Model import Trip
from typing import List

# variables
filteredListFrom = list()
filteredListTo = list()
filteredListBefore = list()
filteredListAfter = list()

weekdayList = {"monday", "tuesday", "wednesday",
               "thursday", "friday", "saturday", "sunday"}

currTime = datetime.datetime.now().time()

today = datetime.datetime.today()

weekdayToday = calendar.day_name[today.weekday()].lower()

# common functions


def loadFile(path: str):
    trips = list()
    with open(path, "r") as d:
        jsondata = json.load(d)

    for item in jsondata:
        trips.append(Trip.RenderedTrip.parse_obj(item))
    return trips


def convertDateToWeekday(date):
    return calendar.day_name[date.weekday()].lower()


def isDepartTimeTime(trip: Trip.RenderedTrip):
    if type(trip.departTime) == datetime.time:
        return True
    return False


def isDepartTimeDateTime(trip: Trip.RenderedTrip):
    if type(trip.departTime) == datetime.datetime:
        return True
    return False


def isDepartTimeNone(trip: Trip.RenderedTrip):
    if trip.departTime == None:
        return True
    return False


def isDepartDateNone(trip: Trip.RenderedTrip):
    if trip.departDate == None:
        return True
    return False


def isDepartDateDate(trip: Trip.RenderedTrip):
    if type(trip.departDate) == datetime.date:
        return True
    return False


def makeTmpDatetime(trip: Trip.RenderedTrip):
    return datetime.datetime.combine(trip.departDate, trip.departTime)


def isWeekdaysEmpty(trip: Trip.RenderedTrip):
    if trip.weekdays == None or len(trip.weekdays) == 0:
        return True
    return False


# iframe functions
def convertKMtoDegree(radius):
    return radius/111.111


def createTopRightPoint(config):
    return IFrameConstraintsModel.BoundaryPoint(lat=config.lat+convertKMtoDegree(config.radius), lon=config.lon+convertKMtoDegree(config.radius))


def createBottomLeftPoint(config):
    return IFrameConstraintsModel.BoundaryPoint(lat=config.lat-convertKMtoDegree(config.radius), lon=config.lon-convertKMtoDegree(config.radius))


def isInBoundary(config, stop: Trip.Stop):
    topRightPoint: IFrameConstraintsModel.BoundaryPoint = createTopRightPoint(
        config)
    bottomLeftPoint: IFrameConstraintsModel.BoundaryPoint = createBottomLeftPoint(
        config)

    withinBoundary = True

    if stop.coordinates.lon > topRightPoint.lon:
        withinBoundary = False
    elif stop.coordinates.lat > topRightPoint.lat:
        withinBoundary = False
    elif stop.coordinates.lon < bottomLeftPoint.lon:
        withinBoundary = False
    elif stop.coordinates.lat < bottomLeftPoint.lat:
        withinBoundary = False
    return withinBoundary


def makeDeadlineBefore(config):
    return config.startDate - datetime.timedelta(hours=config.timeWindowBefore)


def makeDeadlineAfter(config):
    return config.endDate + datetime.timedelta(hours=config.timeWindowAfter)


def getRelevantWeekdaysBefore(config: IFrameConstraintsModel.Constraints):
    relevantWeekdays = list()
    deadlineBefore = makeDeadlineBefore(config)
    startDate = config.startDate
    tmpDate = deadlineBefore

    while tmpDate < startDate:
        relevantWeekdays.append(copy.deepcopy(convertDateToWeekday(tmpDate)))
        tmpDate += datetime.timedelta(days=1)
    if tmpDate.time() > startDate.time():
        relevantWeekdays.append(copy.deepcopy(convertDateToWeekday(startDate)))
    return relevantWeekdays


def getRelevantWeekdaysAfter(config: IFrameConstraintsModel.Constraints):
    relevantWeekdays = list()
    deadlineAfter = makeDeadlineAfter(config)
    endDate = config.endDate
    tmpDate = endDate

    while tmpDate < deadlineAfter:
        relevantWeekdays.append(copy.deepcopy(convertDateToWeekday(tmpDate)))
        tmpDate += datetime.timedelta(days=1)
    if tmpDate.time() > deadlineAfter.time():
        relevantWeekdays.append(copy.deepcopy(
            convertDateToWeekday(deadlineAfter)))
    return relevantWeekdays


def isTripInDeadlineBefore(trip: Trip.RenderedTrip, config: IFrameConstraintsModel.Constraints):
    deadlineBefore = makeDeadlineBefore(config)
    withinDeadline = False

    if isDepartTimeNone(trip):
        withinDeadline = False
    elif isDepartTimeDateTime(trip):
        if deadlineBefore <= trip.departTime and trip.departTime <= config.startDate:
            withinDeadline = True
    elif isDepartTimeTime(trip):
        if isDepartDateDate(trip):
            tmpDate = makeTmpDatetime(trip)
            if deadlineBefore <= tmpDate and tmpDate <= config.startDate:
                withinDeadline = True
        if isDepartDateNone(trip):
            if not isWeekdaysEmpty(trip):
                withinDeadline = any(
                    item in trip.weekdays for item in getRelevantWeekdaysBefore(config))
    return withinDeadline


def isTripInDeadlineAfter(trip: Trip.RenderedTrip, config: IFrameConstraintsModel.Constraints):
    deadlineAfter = makeDeadlineAfter(config)
    endWeekday = convertDateToWeekday(config.endDate)
    withinDeadline = False

    if isDepartTimeDateTime(trip):
        if deadlineAfter >= trip.departTime and trip.departTime >= config.endDate:
            withinDeadline = True
    elif isDepartTimeNone(trip):
        withinDeadline = False
    elif isDepartTimeTime(trip):
        if isDepartDateDate(trip):
            tmpDate = makeTmpDatetime(trip)
            if deadlineAfter >= tmpDate and tmpDate >= config.endDate:
                withinDeadline = True
        if isDepartDateNone(trip):
            if not isWeekdaysEmpty(trip):
                withinDeadline = any(
                    item in trip.weekdays for item in getRelevantWeekdaysAfter(config))
    return withinDeadline


def isOld(trip):
    boundary = today - datetime.timedelta(days=500)
    try:
        if trip.lastUpdated.date() < boundary.date():
            return True
        return False
    except AttributeError:
        return False


def setDateTime(trip: Trip.RenderedTrip):
    if isDepartTimeDateTime(trip):
        trip.wholeDatetime = trip.departTime
    else:
        trip.wholeDatetime = datetime.datetime.combine(
            trip.departDate, trip.departTime)


def sortByDate(triplist: List[Trip.RenderedTrip]):
    triplist.sort(key=lambda x: x.wholeDatetime)
# rwu functions


# testing purposes functions

def isDateAndTimeNone(trip):
    dateNone = isDepartDateNone(trip)
    timeNone = isDepartTimeNone(trip)
    if dateNone and timeNone:
        return True
    return False
