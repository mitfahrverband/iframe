import calendar
from datetime import date, datetime, timedelta
import json
from jinja2 import Environment, FileSystemLoader
from Connectors.Model import Trip
from Models.Carpool import Carpool
from modules import filter

def setRenderDate(trip):
    render_date = None
    if filter.isDepartTimeDateTime(trip):
        render_date = trip.departTime.date().strftime("%d.%m.")

    else:
        render_date = trip.departDate.strftime("%d.%m.")

    return render_date

def setRenderTime(trip):
    return trip.departTime.strftime("%H:%M")

def setWholeDate(trip):
    if filter.isDepartTimeDateTime(trip):
        return trip.departTime
    return datetime.combine(trip.departDate, trip.departTime)

#Attahes the correct logo to a trip(with giving the relative filepath to the logo)
def attachLogo(trip):
    if "mifaz" in trip.deeplink:
        trip.logo = "../img/mifaz.png"
    elif "bessermitfahren" in trip.deeplink:
        trip.logo = "../img/bessermitfahren_logo.png"
    elif "blablacar" in trip.deeplink:
        trip.logo = "../img/01 - BlaBlaCar (no background).png"
    else: 
        trip.logo = "../img/icon_ride2go_green_small.jpg"

def renderFile( filename,templatesFolder, templateFile, fromList, toList, extension, config):
    fileloader = FileSystemLoader(templatesFolder)
    env = Environment(loader=fileloader)
    rendered = env.get_template(templateFile).render(fromlist = fromList, to = toList, fromLabel = config.FROMLabel, toLabel = config.TOLabel)
    fileName = f"{filename}.{extension}"
    with open(f"./iframe_html/{fileName}", "w") as f:
        f.write(rendered)

# rwu triplist render

def checkIfInWeekdays(trip, date):
    if filter.isWeekdaysEmpty(trip):
        raise Exception("Empty weekdays attribute")
    dateWeekday = filter.convertDateToWeekday(date)
    counter = 0
    while counter < 7:
        if dateWeekday in trip.weekdays and trip.departTime >= date.time():
            return filter.convertDateToWeekday(date+timedelta(days=counter))
        if dateWeekday in trip.weekdays and trip.departTime < date.time():
            secCounter = 1
            while secCounter <= 7:
                tmpDate = date+timedelta(days=counter)+timedelta(days=secCounter)
                tmpWeekday = filter.convertDateToWeekday(tmpDate)
                if tmpWeekday in trip.weekdays:
                    return filter.convertDateToWeekday(tmpDate)
                secCounter+=1
        counter+=1
        dateWeekday = filter.convertDateToWeekday(date+timedelta(days=counter))
    return "No match"

def getDateFromWeekdays(trip, date):
    if filter.isWeekdaysEmpty(trip):
        raise Exception("Empty weekdays attribute")
    dateWeekday = filter.convertDateToWeekday(date)
    counter = 0
    while counter < 7:
        if dateWeekday in trip.weekdays and trip.departTime >= date.time():
            return date+timedelta(days=counter)
        if dateWeekday in trip.weekdays and trip.departTime < date.time():
            secCounter = 1
            while secCounter <= 7:
                tmpDate = date+timedelta(days=counter)+timedelta(days=secCounter)
                tmpWeekday = filter.convertDateToWeekday(tmpDate)
                if tmpWeekday in trip.weekdays:
                    return tmpDate
                secCounter+=1
        counter+=1
        dateWeekday = filter.convertDateToWeekday(date+timedelta(days=counter))
    return None
    
    
def setRenderDateRWU(trip, lang, langList):
    today = datetime.now()
    if not filter.isWeekdaysEmpty(trip):
        if checkIfInWeekdays(trip, today) != "No match":
            return langList[lang][checkIfInWeekdays(trip, today)]
    if filter.isDepartTimeDateTime(trip):
        return trip.departTime.strftime("%d.%m.")
    if filter.isDepartDateDate(trip):
        return trip.departDate.strftime("%d.%m.")

    
def setWholeDateRWU(trip):
    today = datetime.now()
    if filter.isDepartTimeDateTime(trip):
        return trip.departTime
    if not filter.isWeekdaysEmpty(trip):
        return datetime.combine(getDateFromWeekdays(trip, today).date(), trip.departTime)
    return datetime.combine(trip.departDate, trip.departTime)

def renderRWUTriplist(language, weekdayList, templatesFolder, templateFile, tripList, extension):
    fileloader = FileSystemLoader(templatesFolder)
    env = Environment(loader=fileloader)
    rendered = env.get_template(templateFile).render(list = tripList,title=language, weekdays = weekdayList, lang = language)
    fileName = f"triplist_{language}.{extension}"
    with open(f"./htmls/{fileName}", "w") as f:
        f.write(rendered)