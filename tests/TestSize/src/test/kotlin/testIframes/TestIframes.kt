package testIframes

import org.concordion.api.AfterSuite
import org.concordion.api.extension.Extension
import org.concordion.ext.StoryboardExtension
import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import seleniumScreenshotTaker.Browser
import seleniumScreenshotTaker.SeleniumScreenshotTaker
import java.lang.Thread.sleep

@RunWith(ConcordionRunner::class)

class TestIframes {
    var browser = Browser()
    var driver: WebDriver = browser.driver
    var js = driver as JavascriptExecutor

    @Extension
    private val storyBoard = StoryboardExtension()
    private val screenshotTaker = SeleniumScreenshotTaker(browser)

    init{
        driver.get("http://bub.mitfahrverband.de/readme.html")
        storyBoard.setScreenshotTaker(screenshotTaker)
    }

    fun switchToIframe(width: Int){
        driver.switchTo().frame(driver.findElement(By.cssSelector("#iframe-${width}w")))
    }

    fun scrollPage(width: Int){
        var scrl = driver.findElement(By.cssSelector("#iframe-${width}w"))
        js.executeScript("arguments[0].scrollIntoView()",scrl)
        sleep(1500)
    }

    fun clickTo(width: Int): Boolean {
        switchToIframe(width)
        var button = driver.findElement(By.cssSelector(".btn.to"))
        if(button.getAttribute("class").contains("clicked")){
            return false
        }
        button.click()
        driver.switchTo().defaultContent()
        sleep(1000)
        return true
    }

    fun clickFrom(width: Int): Boolean {
        switchToIframe(width)
        var button = driver.findElement(By.cssSelector(".btn.from"))
        if(button.getAttribute("class").contains("clicked")){
            return false
        }
        button.click()
        driver.switchTo().defaultContent()
        sleep(1000)
        return true
    }

    fun scrollDown(width: Int){
        switchToIframe(width)
        var bottom = driver.findElement(By.cssSelector(".green-line"))
        js.executeScript("window.scrollBy(5,document.body.scrollHeight)")
        sleep(3000)
        storyBoard.addScreenshot("Iframe when scrolled down.","Width: $width")
        driver.switchTo().defaultContent()
    }

    fun clickOnTop(width: Int){
        switchToIframe(width)
        var button = driver.findElement(By.cssSelector("#top-BTN"))
        button.click()
        sleep(1500)
        storyBoard.addScreenshot("After clicked the scroll back to top button", "Width: $width")
        driver.switchTo().defaultContent()
    }
    fun setFullscreen(){
        driver.manage().window().maximize()
    }

    @AfterSuite
    fun close(){
        driver.quit()
    }



}