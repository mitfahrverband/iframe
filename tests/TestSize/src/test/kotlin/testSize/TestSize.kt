package testSize

import org.concordion.api.AfterSuite
import org.concordion.api.extension.Extension
import org.concordion.ext.StoryboardExtension
import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import org.openqa.selenium.By
import org.openqa.selenium.Dimension
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import seleniumScreenshotTaker.Browser
import seleniumScreenshotTaker.SeleniumScreenshotTaker
import java.lang.Thread.sleep

@RunWith(ConcordionRunner::class)


class TestSize {
    var browser = Browser()
    var driver: WebDriver = browser.driver
    var wait = WebDriverWait(driver, 30)
    var js = driver as JavascriptExecutor

    @Extension
    private val storyBoard = StoryboardExtension()
    private val screenshotTaker = SeleniumScreenshotTaker(browser)

    init{
        driver.get("http://bub.mitfahrverband.de/")
        storyBoard.setScreenshotTaker(screenshotTaker)
    }

    fun clickTO(): Boolean{
        var button = driver.findElement(By.cssSelector(".btn.to"))
        if(button.getAttribute("class").contains("clicked")){
            return false
        }
        else {
            button.click()
            return true
        }

    }

    fun clickFROM(): Boolean{
        var button = driver.findElement(By.cssSelector(".btn.from"))
        if(button.getAttribute("class").contains("clicked")){
            return false
        }
        else {
            button.click()
            return true
        }
    }

    fun scrollTop(){
        var top = driver.findElement(By.className("header"))
        js.executeScript("arguments[0].scrollIntoView()", top)
        sleep(1000)
    }
    fun setFullscreen(){
        driver.manage().window().maximize()
    }

    fun setSize(width: Int, height: Int){
        driver.manage().window().setSize(Dimension(width,height))
    }
    fun screenshotFullscreen(): Boolean{
        storyBoard.addScreenshot("Fullscreen screenshot","This is how the iframe looks like on fullscreen")
        var footer = driver.findElement(By.className("green-line"))
        js.executeScript("arguments[0].scrollIntoView()", footer)
        sleep(5000)
        if(storyBoard.addScreenshot("Fullscreen bottom", "This is how the iframe looks like on the bottom")!=null) return true
        return false
    }

    fun screenshot(width: String, height: String): Boolean{
        scrollTop()
        var width2 = width.trim().toInt()
        var height2 = height.trim().toInt()
        setSize(width2, height2)
        sleep(1000)
        storyBoard.addScreenshot("Size:$width x $height","This is how the iframe looks like on fullscreen")
        if(!clickTO()){
            clickFROM()
        }
        storyBoard.addScreenshot("Button clicked","This is how looks like when the other button is clicked")
        var footer = driver.findElement(By.className("green-line"))
        js.executeScript("arguments[0].scrollIntoView()", footer)
        sleep(5000)
        if(storyBoard.addScreenshot("Size:$width x $height", "This is how the iframe looks like on the bottom")!=null) return true
        return false
    }

    @AfterSuite
    fun close(){
        driver.quit()
    }
}