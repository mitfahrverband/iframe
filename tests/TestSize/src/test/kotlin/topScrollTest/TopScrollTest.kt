package topScrollTest

import org.concordion.api.AfterSuite
import org.concordion.api.extension.Extension
import org.concordion.ext.StoryboardExtension
import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import org.openqa.selenium.By
import org.openqa.selenium.Dimension
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import seleniumScreenshotTaker.Browser
import seleniumScreenshotTaker.SeleniumScreenshotTaker
import java.lang.Thread.sleep

@RunWith(ConcordionRunner::class)

class TopScrollTest {
    var browser = Browser()
    var driver: WebDriver = browser.driver
    var js = driver as JavascriptExecutor

    @Extension
    private val storyBoard = StoryboardExtension()
    private val screenshotTaker = SeleniumScreenshotTaker(browser)

    init{
        driver.get("https://ridewithukraine.org/iframe/bits_baume_konferenz.html")
        storyBoard.setScreenshotTaker(screenshotTaker)
    }

    fun setFullScreen(){
        driver.manage().window().maximize()
    }

    fun takeScreenshot(){
        storyBoard.addScreenshot("Page when it is loaded.", "")
    }

    fun scrollDown(){
        var bottom = driver.findElement(By.className("green-line"))
        js.executeScript("arguments[0].scrollIntoView()",bottom)
        sleep(1500)
        storyBoard.addScreenshot("Page when scrolled down.","")
    }

    fun clickOnTop(){
        var button = driver.findElement(By.id("top-BTN"))
        button.click()
        sleep(1500)
        storyBoard.addScreenshot("After clicked the button", "")
    }

    fun isTop() : Boolean{
        var header = driver.findElement(By.cssSelector(".header>a"))
        if (header.isDisplayed){
            return true
        }
        return false
    }

    fun setSize(width: Int, height: Int){
        driver.manage().window().setSize(Dimension(width,height))
    }

    @AfterSuite
    fun close(){
        driver.quit()
    }
}