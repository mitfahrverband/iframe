## [Test for Iframes in different sizes.]()
[](- "setFullscreen()")

**[300](- "#width") width check**

[](- "scrollPage(#width)")
First we [click from button](- "c:assert-true=clickFrom(#width)") and then [click to button](- "c:assert-true=clickTo(#width)").
After that we [scroll down](- "scrollDown(#width)") and then [click on scroll back to top button](- "clickOnTop(#width)") to scroll up.

**[600](- "#width") width check**

[](- "scrollPage(#width)")
First we [click from button](- "c:assert-true=clickFrom(#width)") and then [click to button](- "c:assert-true=clickTo(#width)").
After that we [scroll down](- "scrollDown(#width)") and then [click on scroll back to top button](- "clickOnTop(#width)") to scroll up.

**[900](- "#width") width check**

[](- "scrollPage(#width)")
First we [click from button](- "c:assert-true=clickFrom(#width)") and then [click to button](- "c:assert-true=clickTo(#width)").
After that we [scroll down](- "scrollDown(#width)") and then [click on scroll back to top button](- "clickOnTop(#width)") to scroll up.