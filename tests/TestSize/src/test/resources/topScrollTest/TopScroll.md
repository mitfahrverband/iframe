##[Top Scroll Test]()
First, it takes a screenshot when the page is loaded in fullscreen. [](- "takeScreenshot()")
Then [set the size full screen](- "setFullScreen()"), after that it [scrolls down to the bottom](- "scrollDown()").
Then it will [click](- "clickOnTop()") on the top button, and it will [scroll to the top of the page](- "c:assert-true=isTop()").

Test when the page width is [580](- "#width") and height is [600](- "#height") [](- "setSize(#width, #height)").
[Take a screenshot](- "takeScreenshot()"), then [scrolls down to the bottom](- "scrollDown()").
Then it will [click](- "clickOnTop()") on the top button, and it will [scroll to the top of the page](- "c:assert-true=isTop()").