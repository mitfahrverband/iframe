##[TestSize](-)

We set the screen size to [fullscreen](- "setFullscreen()") and [take a screenshot of it](- "c:assert-true=screenshotFullscreen()").

Then we repeat the same with different sizes:

| [](- "#result=screenshot(#width, #height)") [Width](- "#width") | [Height](- "#height") | [Screenshot made](- "c:assert-true=#result") |
|-----------------------------------------------------------------|-----------------------|----------------------------------------------|
| 1920                                                            | 600                   |                                              |
| 600                                                             | 600                   |                                              |
| 800                                                             | 600                   |                                              |
| 300                                                             | 600                   |                                              |

[](- "setFullscreen()")
