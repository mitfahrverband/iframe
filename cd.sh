#!/bin/bash


while true
do
    started=$(date +%T)
    echo "Start time: $started"
    git pull
    pip3 install -r requirements.txt
    
    cd Connectors
    
    ./download.sh
    cd ..
    
    sh generate.sh
    
    cd iframe_html
    
    ./upload.sh
    echo "Uploaded"
    
    cd ~/iframe
    
    ended=$(date +%T)
    echo "End time: $ended"
    
    sleep 300
    
done
