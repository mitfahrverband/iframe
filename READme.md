# To set up the project:
- check if python is installed
- check if pip is installed
- create a new directory
- ``` git clone https://codeberg.org/mitfahrverband/iframe ```
- create a new virtual environment: ``` python -m venv env ```
- start the new virtual environment ``` source env/bin/activate ```
- run ``` pip install -r requirements.txt ``` or ``` pip3 install -r requirements.txt ```



### Create a new directory for the htmls(iframe_html) and for the data(json_data) also for the data gained from the connectors.

To make the directories:

```
mkdir json_data
mkdir iframe_html
mkdir Connectors/data/trips_data
```
**You need a new directory for the query ranking and for the query data:**

```
mkdir Connectors/data/query_data
mkdir Connectors/data/ranking_data
```

### Connectors

To run the connectors you need the ***keys.kdbx*** Keepass database for the API keys.


> When you open the html it won't have any images, for those are not on git. You can just copy the public/imgs directory from the rwu ftp server