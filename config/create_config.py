from ast import arg
import json, pydantic,sys ,datetime
sys.path.append("../Models")

import IFrameConstraintsModel


argv = sys.argv[1:]
print(argv)


config = IFrameConstraintsModel.Constraints(lat=float(argv[0]), lon=float(argv[1]),
                    startDate=datetime.datetime.strptime(argv[2],"%Y-%m-%dT%H:%M"),
                    endDate=datetime.datetime.strptime(argv[3],"%Y-%m-%dT%H:%M"),
                    radius=int(argv[4]),TOLabel=argv[5],FROMLabel=argv[6],timeWindowBefore=int(argv[7]),
                    timeWindowAfter=int(argv[8]))


print(config.json())
config = json.loads(config.json())

with open("config_files/bub.json", "w") as out:
    json.dump(config, out)


